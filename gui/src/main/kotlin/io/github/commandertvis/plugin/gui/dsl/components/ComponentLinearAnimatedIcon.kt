package io.github.commandertvis.plugin.gui.dsl.components

import io.github.commandertvis.plugin.gui.GuiLocation
import io.github.commandertvis.plugin.gui.dsl.GUI
import io.github.commandertvis.plugin.gui.runtimePlugin
import io.github.commandertvis.plugin.server
import org.bukkit.enchantments.Enchantment
import org.bukkit.inventory.ItemStack

/**
 * Represents a linear animated component for [GUI].
 */
public open class ComponentLinearAnimatedIcon<S>(
    /**
     * Interval between changing an icon.
     */
    public val intervalMillis: Long,

    /**
     * Item stacks, rolled in this component.
     */
    public val items: Collection<ItemStack?>
) : GuiComponent<S> {

    init {
        require(items.isNotEmpty()) { "animation must have at least one item" }
    }

    /**
     * Changes the enchantment of all items, rolled in this component.
     *
     * @param enchantment the new enchantments map.
     */
    public open fun enchantment(enchantment: Map<Enchantment, Int>) {
        items {
            val meta = itemMeta ?: return@items
            enchantment.forEach { (enchantment, level) -> meta.addEnchant(enchantment, level, true) }
            itemMeta = meta
        }
    }

    /**
     * Changes the display name of all items, rolled in this component.
     *
     * @param name the new display name.
     */
    public open fun name(name: String): Unit = items {
        val meta = itemMeta ?: return@items
        meta.setDisplayName(name)
        itemMeta = meta
    }

    /**
     * Changes the lore of all items, rolled in this component.
     *
     * @param lore the new lore.
     */
    public open fun lore(lore: List<String>): Unit = items {
        val meta = itemMeta ?: return@items
        meta.lore = lore
        itemMeta = meta
    }

    /**
     * Applies an action to all the items, rolled in this component.
     *
     * @param action the action to apply.
     */
    public inline fun items(action: ItemStack.() -> Unit): Unit = items.forEach { it?.action() }

    public override fun apply(gui: GUI<S>, location: GuiLocation) {
        gui.apply {
            location.element(items.first()) {
                onLoaded {
                    server.scheduler.runTask(runtimePlugin) { _ ->
                        while (isShown) {
                            items.forEach {
                                if (!isShown)
                                    return@runTask

                                gui { location.update { item = it } }
                                updateContents()
                                Thread.sleep(intervalMillis)
                            }
                        }
                    }
                }
            }
        }
    }
}
