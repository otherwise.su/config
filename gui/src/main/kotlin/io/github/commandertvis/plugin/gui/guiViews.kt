package io.github.commandertvis.plugin.gui

import io.github.commandertvis.plugin.gui.dsl.GuiView

@PublishedApi
internal val guiViews: MutableList<GuiView<*>> = mutableListOf()
