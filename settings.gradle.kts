rootProject.name = "plugin-api"
enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")
include("chat-components", "common", "coroutines", "gui", "runtime", "command")
