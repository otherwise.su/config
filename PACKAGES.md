# Package io.github.commandertvis.plugin
General API and plugin types, top-level bindings of Bukkit functions and values.
# Package io.github.commandertvis.plugin.chatcomponents
Chat components API extensions.
# Package io.github.commandertvis.plugin.command
Bukkit command generating utilities.
# Package io.github.commandertvis.plugin.command.contexts
Command contexts parsing API.
# Package io.github.commandertvis.plugin.command.dsl
Bukkit command building DSL classes and functions.
# Package io.github.commandertvis.plugin.command.dsl.help
Help command building DSL classes of command API. (deprecated)
# Package io.github.commandertvis.plugin.gui
Chest inventory GUI API.
# Package io.github.commandertvis.plugin.gui.dsl
DSL builder of GUI objects.
# Package io.github.commandertvis.plugin.gui.dsl.components
Components of GUIs for handling typical GUI elements.
# Package io.github.commandertvis.plugin.json
Bukkit API extension to configure plugins and store information using JSON instead of YAML.
# Package io.github.commandertvis.plugin.json.adapters
Builtin JSON adapters to use in JSON configurations.
# Package io.github.commandertvis.plugin.reply
Language extension for more comfortable string appending and sending messages to command senders.