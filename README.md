# plugin-api [![Pipeline status](https://img.shields.io/gitlab/pipeline/CMDR_Tvis/plugin-api?style=flat-square)](https://gitlab.com/CMDR_Tvis/plugin-api/commits/master) [![License](https://img.shields.io/badge/license-MIT-44be16?style=flat-square)](LICENSE) [![Packages](https://img.shields.io/maven-metadata/v?metadataUrl=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F10077943%2Fpackages%2Fmaven%2Fio%2Fgithub%2Fcommandertvis%2Fplugin%2Fcommon%2Fmaven-metadata.xml&style=flat-square)](https://gitlab.com/CMDR_Tvis/plugin-api/-/packages) [![Documentation](https://img.shields.io/website?down_message=offline&label=documentation&style=flat-square&up_message=online&url=https%3A%2F%2Fcmdr_tvis.gitlab.io%2Fplugin-api)](https://cmdr_tvis.gitlab.io/plugin-api)

Versatile plugin utilities for your Spigot plugins in Kotlin:

* type-safe builders of various API objects,
* concise event handling,
* GUI API and DSL,
* commands framework,
* minor language extensions,
* runtime bundling of `kotlin-stdlib-jdk8`, `kotlin-reflect`, and `kotlinx-coroutines-core`,
* JSON-based configuration API (Gson and Jackson from the box),
* coroutine dispatchers and asynchronous event handling.

## Runtime

Download the [API runtime](https://gitlab.com/api/v4/projects/10077943/packages/maven/io/github/commandertvis/plugin/runtime/18.0.0/runtime-18.0.0.jar) and put it into your `/plugins/` folder alongside all plugins depending on it.

## Compatibility with Spigot API

Most functionalities of the API was tested for 1.8.8 and even some hacks are made for compatibility with older versions; however, there still can be problems&mdash;report them, please.

## Including

The library is divided into five modules with different purposes. You may include all the APIs you need; however, each module depends on `common` anyway.

There are snippets for Gradle and Maven build tools.

> <details>
> <summary>Gradle Groovy DSL</summary>
> <pre>repositories {
>     mavenCentral()
>     maven { url 'https://gitlab.com/api/v4/projects/10077943/packages/maven' }
>     maven { url 'https://hub.spigotmc.org/nexus/content/repositories/snapshots/' }
>     maven { url 'https://oss.sonatype.org/content/repositories/snapshots' }
> }<br/>
> dependencies {
>     compileOnly 'org.spigotmc:spigot-api:1.18.1-R0.1-SNAPSHOT'
>     compileOnly 'io.github.commandertvis:chat-components:18.0.0'
>     compileOnly 'io.github.commandertvis:command:18.0.0'
>     compileOnly 'io.github.commandertvis:common:18.0.0'
>     compileOnly 'io.github.commandertvis:coroutines:18.0.0'
>     compileOnly 'io.github.commandertvis:gui:18.0.0'
> }
> </pre>
> </details>
>
> <details>
> <summary>Gradle Kotlin DSL</summary>
> <pre>repositories {
>     mavenCentral()
>     maven("https://gitlab.com/api/v4/projects/10077943/packages/maven")
>     maven("https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
>     maven("https://oss.sonatype.org/content/repositories/snapshots")
> }<br/>
> dependencies {
>     compileOnly("org.spigotmc:spigot-api:1.18.1-R0.1-SNAPSHOT")
>     compileOnly("io.github.commandertvis:chat-components:18.0.0")
>     compileOnly("io.github.commandertvis:command:18.0.0")
>     compileOnly("io.github.commandertvis:common:18.0.0")
>     compileOnly("io.github.commandertvis:coroutines:18.0.0")
>     compileOnly("io.github.commandertvis:gui:18.0.0")
> }
> </pre>
> </details>
>
> <details>
> <summary>Maven POM</summary>
> <pre>&lt;repositories&gt;
>     &lt;repository&gt;
>         &lt;id&gt;pluginApiGitlab&lt;/id&gt;
>         &lt;url&gt;https://gitlab.com/api/v4/projects/10077943/packages/maven&lt;/url&gt;
>     &lt;/repository&gt;
>     &lt;repository&gt;
>         &lt;id&gt;spigotNexus&lt;/id&gt;
>         &lt;url&gt;https://hub.spigotmc.org/nexus/content/repositories/snapshots/&lt;/url&gt;
>     &lt;/repository&gt;
>     &lt;repository&gt;
>         &lt;id&gt;sonatypeOss&lt;/id&gt;
>         &lt;url&gt;https://oss.sonatype.org/content/repositories/snapshots&lt;/url&gt;
>     &lt;/repository&gt;
> &lt;/repositories&gt;
> &lt;dependencies&gt;
>    &lt;dependency&gt;
>         &lt;groupId&gt;org.spigotmc&lt;/groupId&gt;
>         &lt;artifactId&gt;spigot-api&lt;/artifactId&gt;
>         &lt;version&gt;${spigotApi.version}&lt;/version&gt;
>         &lt;scope&gt;provided&lt;/scope&gt;
>     &lt;/dependency&gt;
>     &lt;dependency&gt;
>         &lt;groupId&gt;io.github.commandertvis.plugin&lt;/groupId&gt;
>         &lt;artifactId&gt;common&lt;/artifactId&gt;
>         &lt;version&gt;18.0.0&lt;/version&gt;
>         &lt;scope&gt;provided&lt;/scope&gt;
>     &lt;/dependency&gt;
>     &lt;dependency&gt;
>         &lt;groupId&gt;io.github.commandertvis.plugin&lt;/groupId&gt; 
>         &lt;artifactId&gt;command&lt;/artifactId&gt;
>         &lt;version&gt;18.0.0&lt;/version&gt;
>         &lt;scope&gt;provided&lt;/scope&gt;
>     &lt;/dependency&gt;
>     &lt;dependency&gt;
>         &lt;groupId&gt;io.github.commandertvis.plugin&lt;/groupId&gt; 
>         &lt;artifactId&gt;coroutines&lt;/artifactId&gt;
>         &lt;version&gt;18.0.0&lt;/version&gt;
>         &lt;scope&gt;provided&lt;/scope&gt;
>     &lt;/dependency&gt;
>     &lt;dependency&gt;
>         &lt;groupId&gt;io.github.commandertvis.plugin&lt;/groupId&gt;
>         &lt;artifactId&gt;chat-components&lt;/artifactId&gt;
>         &lt;version&gt;18.0.0&lt;/version&gt;
>         &lt;scope&gt;provided&lt;/scope&gt;
>     &lt;/dependency&gt;
>     &lt;dependency&gt;
>         &lt;groupId&gt;io.github.commandertvis.plugin&lt;/groupId&gt;
>         &lt;artifactId&gt;gui&lt;/artifactId&gt;
>         &lt;version&gt;${pluginApi.version}&lt;/version&gt;
>         &lt;scope&gt;provided&lt;/scope&gt;
>     &lt;/dependency&gt;
> &lt;/dependencies&gt;
> </pre>
> </details>

## Licensing

This project is licensed under the [MIT license](LICENSE).

## Examples

Examples are published in the separate project: CMDR_Tvis/plugin-api-examples>. Feel free to report [an issue](https://gitlab.com/CMDR_Tvis/plugin-api/-/issues) to request an example for a certain API!

## Documentation

API documentation is published [here](http://cmdr_tvis.gitlab.io/plugin-api).
