package io.github.commandertvis.plugin.command.contexts

import java.math.BigInteger

/**
 * The [io.github.commandertvis.plugin.command.contexts.CommandContext] implementation for [BigInteger] type.
 */
public object BigIntegerContext : OneStringContext<BigInteger>() {
    public override fun resolve(string: String): BigInteger? = string.toBigIntegerOrNull()
}
