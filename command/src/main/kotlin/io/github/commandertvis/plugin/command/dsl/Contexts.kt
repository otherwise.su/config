package io.github.commandertvis.plugin.command.dsl

import io.github.commandertvis.plugin.command.contexts.CommandContext
import kotlin.reflect.KClass

/**
 * Lambda Annotation to match parameters to their [CommandContext] classes.
 *
 * @property types The array of [CommandContext] classes.
 */
@Retention(AnnotationRetention.RUNTIME)
public annotation class Contexts(vararg val types: KClass<out CommandContext<*>> = [])
