package io.github.commandertvis.plugin.command.contexts

import io.github.commandertvis.plugin.findOfflinePlayer
import io.github.commandertvis.plugin.toUuidOrNull
import org.bukkit.OfflinePlayer

/**
 * The [io.github.commandertvis.plugin.command.contexts.CommandContext] implementation for [OfflinePlayer] type.
 */
public object OfflinePlayerContext : OneStringContext<OfflinePlayer>() {
    public override fun resolve(string: String): OfflinePlayer? {
        findOfflinePlayer(string)?.let { return it }
        findOfflinePlayer(string.toUuidOrNull() ?: return null).let { return it }
    }
}
