package io.github.commandertvis.plugin.command.contexts

import java.math.BigDecimal

/**
 * The [io.github.commandertvis.plugin.command.contexts.CommandContext] implementation for [BigDecimal] type.
 */
public object BigDecimalContext : OneStringContext<BigDecimal>() {
    public override fun resolve(string: String): BigDecimal? = string.toBigDecimalOrNull()
}
