package io.github.commandertvis.plugin

import io.github.commandertvis.plugin.command.dsl.CommandScope
import org.bukkit.plugin.Plugin
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

/**
 * Builds a new command, applies data there and registers.
 *
 * For example:
 *
 * ```
 * command("plugin", "command") {
 *      description = "The useful command"
 *      execution { default { _, _ -> sendMessage("You've accessed the useful command") } }
 * }
 * ```
 *
 * @param prefix the command's fallback prefix.
 * @param name the command's name - single alias.
 * @return new [CommandScope] reference.
 */
public inline fun command(prefix: String, name: String, block: CommandScope.() -> Unit): CommandScope {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }
    return command(prefix, setOf(name), block)
}

/**
 * Builds a new command, applies data there and registers.
 *
 * For example:
 *
 * ```
 * command("plugin", setOf("command", "cmd")) {
 *      description = "The useful command"
 *      execution { default { _, _ -> sendMessage("You've accessed the useful command") } }
 * }
 * ```
 *
 * @param prefix the command's fallback prefix.
 * @param aliases the command's aliases.
 * @return the new [CommandScope] reference.
 */
public inline fun command(prefix: String, aliases: Set<String>, block: CommandScope.() -> Unit): CommandScope {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }

    return CommandScope(prefix, aliases).apply {
        block()
        injectToBukkit()
    }
}

/**
 * Builds a new command, applies data there and registers. The plugin's name acts as fallback prefix.
 *
 * For example:
 *
 * ```
 * plugin.command("command") {
 *      description = "The useful command"
 *      execution { default { _, _ -> sendMessage("You've accessed the useful command") } }
 * }
 * ```
 *
 * @param name the command's name - single alias.
 * @return new [CommandScope] reference.
 */
public inline fun Plugin.command(name: String, block: CommandScope.() -> Unit): CommandScope {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }
    return command(setOf(name), block)
}

/**
 * Builds a new command, applies data there and registers. The plugin's name acts as fallback prefix.
 *
 * For example:
 *
 * ```
 * plugin.command(setOf("command", "cmd")) {
 *      description = "The useful command"
 *      execution { default { _, _ -> sendMessage("You've accessed the useful command") } }
 * }
 * ```
 *
 * @param aliases the command's aliases.
 * @return the new [CommandScope] reference.
 */
public inline fun Plugin.command(aliases: Set<String>, block: CommandScope.() -> Unit): CommandScope {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }
    return command(name.lowercase(), aliases, block)
}
