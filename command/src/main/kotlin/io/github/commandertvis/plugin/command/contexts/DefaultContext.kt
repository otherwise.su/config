package io.github.commandertvis.plugin.command.contexts

/**
 * Default context is just maps given array to [List]. Its length is unknown.
 */
public object DefaultContext : CommandContext<List<String>> {
    override val length: Nothing
        get() = throw UnsupportedOperationException("DefaultContext does not have fixed length.")

    override fun resolve(strings: Array<String>): List<String>? = strings.asList()
}
