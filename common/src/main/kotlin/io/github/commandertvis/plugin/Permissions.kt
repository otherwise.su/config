package io.github.commandertvis.plugin

import org.bukkit.permissions.Permission

/**
 * This function adds permission to the Bukkit [org.bukkit.plugin.PluginManager].
 */
public fun Permission.register(): Unit = server.pluginManager.addPermission(this)
