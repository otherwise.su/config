package io.github.commandertvis.plugin

import org.bukkit.inventory.Recipe

/**
 * Adds this [Recipe] to the crafting manager.
 *
 * @return true if the recipe was added, false if it wasn't for some reason.
 */
public fun Recipe.add(): Boolean = server.addRecipe(this)
