package io.github.commandertvis.plugin

import org.bukkit.event.Cancellable
import org.bukkit.event.Event

/**
 * Sets this event cancelled state to `true`.
 *
 * @param T the type of event.
 */
public fun <T> T.cancel(): Unit where T : Cancellable, T : Event = run { isCancelled = true }
