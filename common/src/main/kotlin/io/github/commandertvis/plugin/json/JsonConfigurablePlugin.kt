package io.github.commandertvis.plugin.json

import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.plugin.Plugin
import org.bukkit.plugin.java.JavaPlugin
import java.io.File

/**
 * Represents a [Plugin] that can use configurations, managed using JSON. All the YAML related behavior is overridden
 * with same actions with JSON configuration.
 *
 * @param C the structure of this configuration.
 */
public abstract class JsonConfigurablePlugin<C : Any> public constructor(
    public final override val defaultJsonConfig: C,
) : JavaPlugin(), JsonConfigurationManager<C> {
    private val lazyJsonConfig = LazyJsonConfigurationValue<C, JsonConfigurablePlugin<C>>()

    /**
     * Returns path to `config.json` in [dataFolder].
     */
    public final override val jsonConfigFilePath: String
        get() = jsonConfigFile.path

    public override val jsonConfigFile: File
        get() = File(
            dataFolder.apply {
                if (!exists())
                    mkdirs()
            },

            "config.json"
        ).apply {
            if (!exists())
                createNewFile()
        }

    /**
     * Retrieves the configuration, parsed with [C] type from file contents, delegated with [lazyJsonConfig].
     */
    public override var jsonConfig: C by lazyJsonConfig

    /**
     * Saves the current JSON configuration to the [jsonConfigFile].
     */
    public override fun saveConfig(): Unit = saveJsonConfiguration(jsonConfig)

    /**
     * Saves the default JSON configuration to the [jsonConfigFile].
     */
    public override fun saveDefaultConfig(): Unit = run { getJsonConfiguration() }

    /**
     * Creates a [YamlConfiguration], parsed of [jsonConfig] contents, because JSON is valid YAML.
     *
     * @return the new [YamlConfiguration] reference.
     */
    public override fun getConfig(): YamlConfiguration =
        YamlConfiguration().apply { loadFromString(jsonConfigFile.readText()) }

    /**
     * Reloads the [jsonConfig] from the contents of [jsonConfigFile].
     */
    public override fun reloadConfig(): Unit = lazyJsonConfig.reload(this)
}
