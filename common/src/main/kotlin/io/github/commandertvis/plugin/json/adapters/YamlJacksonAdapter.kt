package io.github.commandertvis.plugin.json.adapters

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule

/**
 * A [JsonAdapter] implementation, which uses [YAMLMapper] for serialization and deserialization.
 */
public open class YamlJacksonAdapter(
    mapper: YAMLMapper = YAMLMapper().apply {
        configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        configure(YAMLGenerator.Feature.MINIMIZE_QUOTES, true)
        configure(YAMLGenerator.Feature.SPLIT_LINES, true)
        registerModule(JavaTimeModule())
        registerKotlinModule()
    }
) : JacksonAdapter(mapper) {
    public override fun toJson(any: Any): String? = mapper.writeValueAsString(any)

    public companion object : YamlJacksonAdapter()
}

