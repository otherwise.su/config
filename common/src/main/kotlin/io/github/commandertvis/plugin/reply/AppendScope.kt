package io.github.commandertvis.plugin.reply

/**
 * Represents an [Appendable] context extension to be able to [append] with Kotlin `unaryPlus` operator.
 */
public class AppendScope<T : Appendable>(
    /**
     * The [Appendable] object used.
     */
    public val appendable: T
) : Appendable by appendable {
    /**
     * Appends an object to the [StringBuilder] by an operator
     */
    public operator fun Any?.unaryPlus(): Unit = run { appendable.append(this.toString()) }
}
