package io.github.commandertvis.plugin

import org.bukkit.Location
import org.bukkit.StructureType
import org.bukkit.World
import org.bukkit.generator.ChunkGenerator
import org.bukkit.inventory.ItemStack
import org.bukkit.map.MapView

/**
 * Creates a [org.bukkit.generator.ChunkGenerator.ChunkData] for use in a generator.
 *
 * @return a new [org.bukkit.generator.ChunkGenerator.ChunkData] for the world.
 */
public fun World.createChunkData(): ChunkGenerator.ChunkData = server.createChunkData(this)

/**
 * Create a new explorer map targeting the closest nearby structure of a given [StructureType].
 *
 * This method uses implementation default values for radius and findUnexplored (usually 100, true).
 *
 * @param location the origin location to find the nearest structure.
 * @param structureType the type of structure to find.
 * @return the newly created item stack.
 * @see World.locateNearestStructure
 */
public fun World.createExplorerMap(location: Location, structureType: StructureType): ItemStack =
    server.createExplorerMap(this, location, structureType)

/**
 * Create a new explorer map targeting the closest nearby structure of a given [StructureType].
 *
 * This method uses implementation default values for the radius and findUnexplored (usually 100, true).
 *
 * @param location the origin location to find the nearest structure.
 * @param structureType the type of structure to find.
 * @param radius radius to search, see World#locateNearestStructure for more information.
 * @param findUnexplored whether to find unexplored structures.
 * @return the newly created item stack.
 * @see World.locateNearestStructure
 */
public fun World.createExplorerMap(
    location: Location,
    structureType: StructureType,
    radius: Int,
    findUnexplored: Boolean
): ItemStack = server.createExplorerMap(this, location, structureType, radius, findUnexplored)

/**
 * Create a new map of this [World] with an automatically assigned ID.
 *
 * @return a newly created map view.
 */
public fun World.createMap(): MapView = server.createMap(this)

/**
 * Unloads the given world.
 *
 * @param save whether to save the chunks before unloading.
 * @return true if successful, false otherwise.
 */
public fun World.unload(save: Boolean): Boolean = server.unloadWorld(this, save)
