package io.github.commandertvis.plugin.json

import kotlin.contracts.InvocationKind
import kotlin.contracts.contract
import kotlin.properties.ReadOnlyProperty
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.KProperty
import kotlin.reflect.KProperty1

/**
 * Provides one-way data binding of object property to the [JsonConfigurationManager.jsonConfig]. When the property is
 * modified, the configuration is saved.
 *
 * @receiver the configuration manager.
 * @param target the object property.
 * @return the delegate.
 */
public fun <T, R : Any> JsonConfigurationManager<R>.binding(target: KProperty1<R, T>): ReadOnlyProperty<Any?, T> =
    ReadOnlyProperty { _, _ ->
        target.get(jsonConfig)
    }

/**
 * Provides one-way data binding of object property to the [JsonConfigurationManager.jsonConfig]. When the property is
 * modified, the configuration is saved.
 *
 * @receiver the configuration manager.
 * @param target the object property.
 * @return the delegated.
 */
public fun <T, R : Any> JsonConfigurationManager<R>.binding(
    target: KMutableProperty1<R, T>,
): ReadWriteProperty<Any?, T> = object : ReadWriteProperty<Any?, T> {
    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        target.set(jsonConfig, value)
        saveConfig()
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>): T = target.get(jsonConfig)
}

/**
 * Accesses manager by providing a scope of it, then saving.
 *
 * @receiver the target manager.
 * @param action the scope function.
 * @return the result of action.
 */
public inline fun <T : Any, C : JsonConfigurationManager<T>, R> C.commit(action: C.() -> R): R {
    contract { callsInPlace(action, InvocationKind.EXACTLY_ONCE) }
    val result = action()
    saveConfig()
    return result
}

/**
 * Accesses manager by providing a scope of [JsonConfigurationManager.jsonConfig], then saving.
 *
 * @receiver the target manager.
 * @param action the scope function.
 * @return the result of action.
 */
public inline fun <T : Any, C : JsonConfigurationManager<T>, R> C.commitWithConfig(action: T.() -> R): R {
    contract { callsInPlace(action, InvocationKind.EXACTLY_ONCE) }
    val result = jsonConfig.action()
    saveConfig()
    return result
}
