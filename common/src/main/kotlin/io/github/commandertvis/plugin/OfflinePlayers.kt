package io.github.commandertvis.plugin

import org.bukkit.OfflinePlayer

/**
 * Unique ID of this [OfflinePlayer] as [String]
 */
public inline val OfflinePlayer.uuidString: String
    get() = uniqueId.toString()
