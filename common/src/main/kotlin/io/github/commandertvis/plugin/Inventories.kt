package io.github.commandertvis.plugin

import org.bukkit.Material
import org.bukkit.event.inventory.InventoryType
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.InventoryHolder
import org.bukkit.inventory.ItemStack

/**
 * Creates an empty inventory of type [InventoryType.CHEST] with the specified size.
 *
 * @param size a multiple of 9 as the size of inventory to create.
 * @return a new inventory.
 * @throws IllegalArgumentException if the size is not a multiple of 9.
 */
public fun createInventory(size: Int): Inventory = (null as InventoryHolder?).createInventory(size)

/**
 * Creates an empty inventory with the specified type and title. If the type is [InventoryType.CHEST], the new
 * inventory has a size of 27; otherwise the new inventory has the normal size for its type.
 *
 * It should be noted that some inventory types do not support titles and may not render with said titles on the
 * Minecraft client.
 *
 * [InventoryType.WORKBENCH] will not process crafting recipes if created with this method. Use
 * [org.bukkit.entity.Player.openWorkbench] instead.
 *
 * [InventoryType.ENCHANTING] will not process item stacks for possible enchanting results. Use
 * [org.bukkit.entity.Player.openEnchanting] instead.
 *
 * @param type the type of inventory to create.
 * @return a new inventory.
 * @throws IllegalArgumentException if the [InventoryType] cannot be viewed.
 * @see InventoryType.isCreatable
 */
public fun createInventory(type: InventoryType): Inventory = (null as InventoryHolder?).createInventory(type)

/**
 * Creates an empty inventory of type [InventoryType.CHEST] with the specified size and title.
 *
 * @param size a multiple of 9 as the size of inventory to create
 * @param title the title of the inventory, displayed when inventory is viewed
 * @return a new inventory
 * @throws IllegalArgumentException if the size is not a multiple of 9
 */
public fun createInventory(size: Int, title: String): Inventory =
    (null as InventoryHolder?).createInventory(size, title)

/**
 * Creates an empty inventory with the specified type and title. If the type
 * is [InventoryType.CHEST], the new inventory has a size of 27; otherwise the new inventory has the normal size for
 * its type.
 *
 * It should be noted that some inventory types do not support titles and may not render with said titles on the
 * Minecraft client.
 *
 * [InventoryType.WORKBENCH] will not process crafting recipes if
 * created with this method. Use [org.bukkit.entity.Player.openWorkbench] instead.
 *
 * [InventoryType.ENCHANTING] will not process {@link ItemStack}s
 * for possible enchanting results. Use [org.bukkit.entity.Player.openEnchanting] instead.
 *
 * @param type the type of inventory to create.
 * @param title the title of the inventory, to be displayed when it is viewed.
 * @return a new inventory.
 * @throws IllegalArgumentException if the [InventoryType] cannot be viewed.
 *
 * @see InventoryType.isCreatable
 */
public fun createInventory(type: InventoryType, title: String): Inventory =
    (null as InventoryHolder?).createInventory(type, title)

/**
 * Removes all stacks in the inventory matching the given [Material].
 *
 * @param material the material to remove.
 */
public operator fun Inventory.minusAssign(material: Material): Unit = remove(material)

/**
 * Removes all stacks in the inventory matching the given stack.
 *
 * This will only match a slot if both the type, and the amount of the
 * stack match.
 *
 * @param item the [ItemStack] to match against.
 */
public operator fun Inventory.minusAssign(item: ItemStack): Unit = remove(item)

/**
 * Returns the [ItemStack] found in the slot at the given index.
 *
 * @param index the index of the Slot's ItemStack to return.
 * @return the ItemStack in the slot.
 */
public operator fun Inventory.get(index: Int): ItemStack? = getItem(index)

/**
 * Stores the [ItemStack] at the given index of the inventory.
 *
 * @param index the index where to put the ItemStack.
 * @param element the ItemStack to set.
 */
public operator fun Inventory.set(index: Int, element: ItemStack?): Unit = setItem(index, element)
