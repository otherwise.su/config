package io.github.commandertvis.plugin

import org.bukkit.event.*
import org.bukkit.plugin.Plugin
import java.lang.reflect.InvocationTargetException

@PublishedApi
internal class EmptyListener : Listener

/**
 * Adds an event handler with certain [Plugin].
 *
 * @param priority the event priority of the handler.
 * @param ignoreCancelled should this handler ignore cancelled [org.bukkit.event.Cancellable] events.
 * @param action the handling function.
 */
public inline fun <reified T> Plugin.handleCancellable(
    priority: EventPriority = EventPriority.NORMAL,
    ignoreCancelled: Boolean = false,
    crossinline action: T.() -> Unit,
): Unit where T : Event, T : Cancellable = handle(
    EmptyListener(),
    ignoreCancelled,
    priority,
    action
)

/**
 * Adds an event handler with certain [Plugin].
 *
 * @param priority the event priority of the handler.
 * @param action the handling function.
 */
public inline fun <reified T : Event> Plugin.handle(
    priority: EventPriority = EventPriority.NORMAL,
    crossinline action: T.() -> Unit,
): Unit = handle(
    EmptyListener(),
    false,
    priority,
    action
)

@PublishedApi
internal inline fun <reified T : Event> Plugin.handle(
    listener: Listener,
    ignoreCancelled: Boolean,
    priority: EventPriority,
    crossinline action: T.() -> Unit,
): Unit = server.pluginManager.registerEvent(
    T::class.java,
    listener,
    priority,

    { _, event ->
        try {
            if (event is T)
                action(event)
        } catch (any: Throwable) {
            if (any is InvocationTargetException)
                throw EventException(any.cause)

            throw EventException(any)
        }
    },

    this,
    ignoreCancelled
)
