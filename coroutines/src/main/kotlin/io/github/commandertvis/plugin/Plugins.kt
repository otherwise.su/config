package io.github.commandertvis.plugin

import io.github.commandertvis.plugin.coroutines.SchedulerAsyncCoroutineDispatcher
import io.github.commandertvis.plugin.coroutines.SchedulerPrimaryCoroutineDispatcher
import kotlinx.coroutines.*
import org.bukkit.event.Cancellable
import org.bukkit.event.Event
import org.bukkit.event.EventPriority
import org.bukkit.plugin.Plugin
import java.util.concurrent.ConcurrentHashMap

private val mainDispatchers = ConcurrentHashMap<Plugin, CoroutineDispatcher>()

/**
 * Provides [CoroutineDispatcher] that runs coroutines in [org.bukkit.scheduler.BukkitScheduler]
 * primary thread.
 */
public val Plugin.mainDispatcher: CoroutineDispatcher
    get() = mainDispatchers.getOrPut(this) { SchedulerPrimaryCoroutineDispatcher(this) }

private val mainScopes = ConcurrentHashMap<Plugin, CoroutineScope>()

/**
 * Provides [CoroutineScope] based on [mainDispatcher].
 */
public val Plugin.mainScope: CoroutineScope
    get() = mainScopes.getOrPut(this) { CoroutineScope(mainDispatcher) + CoroutineName(name) }

private val asyncDispatchers = ConcurrentHashMap<Plugin, CoroutineDispatcher>()

/**
 * Provides [CoroutineDispatcher] that runs coroutines in [org.bukkit.scheduler.BukkitScheduler]
 * asynchronous thread.
 */
public val Plugin.asyncDispatcher: CoroutineDispatcher
    get() = asyncDispatchers.getOrPut(this) { SchedulerAsyncCoroutineDispatcher(this) }

private val asyncScopes = ConcurrentHashMap<Plugin, CoroutineScope>()

/**
 * Provides [CoroutineScope] based on [asyncDispatcher].
 */
public val Plugin.asyncScope: CoroutineScope
    get() = asyncScopes.getOrPut(this) { CoroutineScope(mainDispatcher) + CoroutineName(name) }

/**
 * Adds an event handler with certain [Plugin], the handler is run in [org.bukkit.scheduler.BukkitScheduler]
 * asynchronous thread.
 *
 * @param priority the event priority of the handler.
 * @param ignoreCancelled should this handler ignore cancelled [org.bukkit.event.Cancellable] events.
 * @param action the handling function.
 */
public inline fun <reified T> Plugin.handleCancellableAsync(
    priority: EventPriority = EventPriority.NORMAL,
    ignoreCancelled: Boolean = false,
    crossinline action: suspend T.() -> Unit,
): Unit where T : Event, T : Cancellable = handleCancellable<T>(
    priority,
    ignoreCancelled
) {
    asyncScope.launch { action(this@handleCancellable) }
}

/**
 * Adds an event handler with certain [Plugin], the handler is run in [org.bukkit.scheduler.BukkitScheduler]
 * asynchronous thread.
 *
 * @param priority the event priority of the handler.
 * @param action the handling function.
 */
public inline fun <reified T : Event> Plugin.handleAsync(
    priority: EventPriority = EventPriority.NORMAL,
    crossinline action: suspend T.() -> Unit,
): Unit = handle<T>(
    priority
) {
    asyncScope.launch { action(this@handle) }
}
